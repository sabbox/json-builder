﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;

namespace JsonObjectGenerator {

    public class JsonBuilder {

        public JObject JsonObject { get; } = new JObject();

        public void AddToDocument(string jsonPath, string propertyValue) {

            var splittedPath = jsonPath.Split('.').ToList();

            string propertyName = ExtractPropertyName(splittedPath);
            IList<string> splitedPathNoPropertyName = ExtractPathSegments(splittedPath);

            JObject lastObjectInPath = CreateOrFindLastObject(splitedPathNoPropertyName);
            lastObjectInPath.Add(new JProperty(propertyName, propertyValue));

        }

        private IList<string> ExtractPathSegments(IList<string> splittedPath) {

            IList<string> splitedPathNoPropertyName = new List<string>();

            for (int i = 0; i < splittedPath.Count; ++i) {

                string segment = splittedPath[i];

                if (i + 1 == splittedPath.Count) {
                    break;
                }

                splitedPathNoPropertyName.Add(segment);
            }

            return splitedPathNoPropertyName;
        }

        private string ExtractPropertyName(IList<string> splittedPath) {
            return splittedPath.Last();
        }

        private JObject CreateOrFindLastObject(IList<string> splitedPathNoPropertyName) {

            JObject lastObjectInPath = JsonObject;

            foreach (var segment in splitedPathNoPropertyName) {

                var jToken = lastObjectInPath[segment];

                if (IsValidObject(jToken)) {
                    lastObjectInPath = (JObject)jToken;
                } else if (NonExitedObject(jToken)) {
                    var jObject = new JObject();
                    lastObjectInPath.Add(segment, jObject);
                    lastObjectInPath = jObject;
                } else {
                    //~ JToken has invalid type; e.g. JProperty
                    throw new InvalidOperationException($"JToken is not an JObject: {jToken.Type}");
                }

            }

            return lastObjectInPath;
        }

        private static bool NonExitedObject(JToken jToken) {
            return jToken == null;
        }

        private static bool IsValidObject(JToken jToken) {
            return jToken != null && jToken.Type == JTokenType.Object;
        }

    }

}
