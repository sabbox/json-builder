﻿using System;
using JsonObjectGenerator;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

namespace JsonObjectGeneratorTest {

    [TestFixture]
    public class JsonBuilderTest {

        [Test]
        public void NestedProperties() {

            string jsonPath = "field1.segment1.segment2";
            string value = "value1";

            var jb = new JsonBuilder();
            jb.AddToDocument(jsonPath, value);

            var expected = JObject.Parse("{'field1':{'segment1':{'segment2':'value1'}}}");

            Assert.That(jb.JsonObject.ToString(), Is.EqualTo(expected.ToString()));

        }

        [Test]
        public void SingleProperty() {

            string jsonPath = "field1";
            string value = "value1";

            var jb = new JsonBuilder();
            jb.AddToDocument(jsonPath, value);

            var expected = JObject.Parse("{'field1': 'value1'}");

            Assert.That(jb.JsonObject.ToString(), Is.EqualTo(expected.ToString()));

        }

        [Test]
        public void AddToExistingLastObject() {

            string jsonPath1 = "field1.segment1.segment2";
            string jsonPath2 = "field1.segment1.segment3";
            string value1 = "value1";
            string value2 = "value2";

            var jb = new JsonBuilder();
            jb.AddToDocument(jsonPath1, value1);
            jb.AddToDocument(jsonPath2, value2);

            var expected = JObject.Parse("{'field1':{'segment1':{'segment2':'value1','segment3':'value2'}}}");

            Assert.That(jb.JsonObject.ToString(), Is.EqualTo(expected.ToString()));
        }

        [Test]
        public void AddToExistingIntermediateObject() {

            string jsonPath1 = "field1.segment1.segment2";
            string jsonPath2 = "field1.segment3";
            string value1 = "value1";
            string value2 = "value2";

            var jb = new JsonBuilder();
            jb.AddToDocument(jsonPath1, value1);
            jb.AddToDocument(jsonPath2, value2);

            var expected = JObject.Parse("{'field1':{'segment1':{'segment2':'value1'},'segment3':'value2'}}");

            Assert.That(jb.JsonObject.ToString(), Is.EqualTo(expected.ToString()));
        }

        [Test]
        public void WhenTryToReplaceValueThenThrows() {

            string jsonPath1 = "field1.segment1.segment2";
            string value1 = "value1";
            string value2 = "value2";

            var jb = new JsonBuilder();
            jb.AddToDocument(jsonPath1, value1);

            Assert.Throws<ArgumentException>(() => { jb.AddToDocument(jsonPath1, value2); });

        }

        [Test]
        public void WhenTryToExtendPathThenThrows() {

            string jsonPath1 = "field1.segment1";
            string jsonPath2 = "field1.segment1.property1";
            string value1 = "value1";
            string value2 = "value2";

            var jb = new JsonBuilder();
            jb.AddToDocument(jsonPath1, value1);

            Assert.Throws<InvalidOperationException>(() => { jb.AddToDocument(jsonPath2, value2); });

        }

    }

}
